fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => {

	let list = json.map((todo => {
		return todo.title;
	}))
	console.log({list})
});

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json()) 
.then(response => console.log(response))

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json()) 
.then(response => console.log('The item "' + response.title + '" on the list has a status of ' + response.completed ))


fetch("https://jsonplaceholder.typicode.com/todos", 
	{
		method: "POST",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			completed: false,
			title: "Created To Do List Item",
			userId:1
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body:JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list wit a different data structure",
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: new Date(),
		status: "Complete",
		title: "delectus aut autem",
		userId: 1

	})
})
.then(response => response.json())
.then(json => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "DELETE"
	}	
).then(response => response.json())
.then(json => console.log(json))
